package com.example.toastapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnToast : Button = findViewById(R.id.btnShowToast)

        btnToast.setOnClickListener {
            Toast.makeText(this,"Hi, I'm a toast",Toast.LENGTH_LONG).show()
        }
    }
}